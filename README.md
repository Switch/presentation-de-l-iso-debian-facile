# Présentation de l ISO Debian Facile

Présentation libre office impress de Debian Facile etde son ISO  d'installation.


**Les notes suivantes sont une ébauche pour l'organisation de l'évènement.
Pour participer contactez moi par le forum ci dessous.
L'ébauche de la présentation libre office et une version PDF sont a coté sur ce framagit.**



Appel sur le forum
==================
* Evènement - Présentation de l'ISO Debian Facile (Isère) en préparation -  https://debian-facile.org/viewtopic.php?id=31669

Volontaires
===========
* Switch
* Une personne sur IRC

Diapo de présentation
=====================

Le fichier de présenation
-------------------------
* Faire un diapo libre office, et le mettre dans le git ?
* Démarrage rapide avec LibreOffice Impress - vidéo 8 min - https://youtu.be/NcrUt4CUzaw
* type de police ?

Linux
-----
* Histoire
* Linux est finalement encore peu connu du grand public, alors que ce dernier l’utilise régulièrement. En effet, Linux se cache dans les smartphones, les téléviseurs, les box internet, etc. Presque 70% des pages web servies dans le monde le sont par un serveur Linux ou UNIX !
* Linux équipe un peu plus d'1,5% des ordinateurs personnels mais plus de 82% des smartphones. Android étant un système d’exploitation dont le kernel est un Linux.
* Multiplateforme
* Ouvert
* La philosophie UNIX
* Les distributions GNU/LINUX
* Les environnements de bureaux
* Libre / Open source 
* Shell
* Mini Quizz ☺
Sources : https://www.formatux.fr/formatux-fondamentaux/module-010-presentation/index.html

### A compléter
* Ecologie et libre ( ordinausores etc ) 
	* https://cacommenceparmoi.org/blog/action/linux/
* Politique - alternative au capitalisme ? -
	* Travail : « Entre profit et pouvoir, le capitalisme préfère le pouvoir » ( payant :/ ) https://www.alternatives-economiques.fr/travail-entre-profit-pouvoir-capitalisme-prefere-pouvoir/00085030 
		* analyse sur linuxfr -  https://linuxfr.org/users/niconico/liens/travail-entre-profit-et-pouvoir-le-capitalisme-prefere-le-pouvoir
			* Extraits
				* il faut obliger le capital à lâcher prise sur l’organisation du travail, il y a là un levier extrêmement précieux de reconquête d’un pouvoir d’agir, d’un pouvoir social à partir du travail.
			C’est possible, et cela se voit déjà dans certaines expériences dans les marges et interstices du capitalisme, comme le travail collaboratif sur Internet, les logiciels libres comme Linux, ces formes d’auto-organisation productive à très grande échelle qui sont extrêmement efficaces et qui échappent largement à l’emprise du capital. On a aussi, par exemple, le mouvement des communs coopératifs, notamment en Catalogne. Et les recherches-actions sur la qualité du travail que mènent certaines équipes syndicales. Tout cela produit des expériences sociales innovantes qui pourront disséminer dans le corps social.
	* Genèse et subversion du capitalisme informationnel  -LINUX et les logiciels libres : vers une nouvelle utopie concrète ? https://www.cairn.info/libres-enfants-du-savoir-numerique--9782841620432-page-171.htm
* Sécurité - achats en ligne, données privées etc ?

Debian
------
* Présentation de Debian https://debian-facile.org/doc:debian
	* Qu'est-ce que Debian ?
		La charte de Debian
		L'engagement vis-à-vis de ses utilisateurs
		Les principes du logiciel libre selon Debian
		Les objectifs
		L'installateur de GNU/Linux Debian
		La bibliothèque Debian des logiciels
		Debian et les autres noyaux
		Découvrir ou aider Debian
* Raisons de choisir Debian - https://www.debian.org/intro/why_debian.fr.html
	* sécurité : ex utilisé par Tails
* Donner un exemple de commandes simples pipées pour un résultat utile à tous. Ex: renomage redimension d'image  en masse etc

Debian Facile
-------------
* Présentation de l'asso Debian Facile - https://debian-facile.org/asso.php
	* le site
	* le forum
	* irc
	* les outils
	* chaine peertube
	* les cahiers du débutant
	* la carte debian facile https://debian-facile.org/viewtopic.php?id=20347

ISO DF
------
* Présentation de l'iso - https://debian-facile.org/projets:iso-debian-facile

Visuels
-------
* Couleur du site ( bleu ) - #3876a8

./banniere.png
./banniere.png

* Tux ./Tux.svg
* Tux Debian ( petite taille: / ) ./tux_debian.jpg
* Tux Debian orbite ( fond écran ) ./tux_debian_orbite.png
* Logo Debian ./logo_Debian.svg
* Logo GNU ./gnu-seeklogo.com.svg
* Image Free Software Dealers ! ./Free_Software_Dealers.jpg
* Logo Debian Facile ( le rond  etc ) ./logo_debian_facile.jpg ./logo_debian_facile-transparent.png
* Logo debian facile Wiki ./logo_debian_facile_wiki.png
* tux Debian Facile et ChocoDF ./tux_debian_facile-chocoDF.png
* la carte DF ./carte_debian_facile.png
* clé usb ./linux_usb.png

Préparer atelier installation suite à présentation ?
====================================================
* Demander aux personnes de venir avec leur PC et une clé USB vierge.

Réservation salle
=================
* fablab de la ville (38)

Matériel
========
* Vidéoprojecteur - Switch ou fablab
* sono ?
* Un PC en wifi avec un cache apt cache-apt pour accélérer les installations en direct ?
* télécharger les videos du peertube DF pour montrer les etapes installation rapddement etc ?

Communication
=============
* Relais de la ville et du Fabla
* Guilde de Grenoble
* Debian Facile

Finances
========
* Faire un pot ?
* Vendre des Clefs DF ISO ?

Date de l'évènement
===================
?
